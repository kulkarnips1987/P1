package com.parag;

/**
 * Hello world!
 *
 */
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

import java.io.InputStream;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        // BeanFactory factory = new XmlBeanFactory(new FileSystemResource("spring.xml"));
        ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        Triangle triangle = (Triangle) context.getBean("triangle");
        triangle.draw();
        FileReader reader = (FileReader) context.getBean("fileReader");
        System.out.println(reader.getContent());
    }
}
