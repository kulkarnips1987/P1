package com.parag;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by seli on 19/6/17.
 */
public class FileReader {
    private String filepath;
    private String content;

    public FileReader(String filepath) {
        this.filepath = filepath;
        InputStream is = getClass().getResourceAsStream(this.filepath);
        // this.content = is.toString();
        Scanner s = new Scanner(is).useDelimiter("\\A");
        this.content = s.hasNext() ? s.next() : "";
    }

    public String getFilepath() {
        return filepath;
    }
    public String getContent() {
        return content;
    }
}
